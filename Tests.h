#pragma once
#include "MyClass.h"
#include "gtest/gtest.h"
struct CalcTest : testing::Test
{ 
};
TEST_F(CalcTest, Metods)
{
	//________________________________________
	std::streambuf *psbuf,*prebuf;
	std::ofstream input;
	std::ifstream output;

	prebuf = std::cout.rdbuf();
	psbuf = input.rdbuf();// get file's streambuf
	if (__argc == 2)
	{
		input.open(__argv[1]);
		std::cout.rdbuf(psbuf);
	}
	std::cout << "PUSH 5" << '\n';
	std::cout << "PRINT" << '\n'; //выводится 5
	std::cout << "PUSH 5.768" << '\n';
	std::cout << "+" << '\n';
	std::cout << "PRINT" << '\n'; //выводится 10.768
	std::cout << "PUSH 10" << '\n';
	std::cout << "-" << '\n';
	std::cout << "PRINT" << '\n'; //выводится  0.768
	std::cout << "PUSH 1000" << '\n';
	std::cout << "*" << '\n';
	std::cout << "PRINT" << '\n'; //выводится  768
	std::cout << "PUSH 2" << '\n';
	std::cout << "/" << '\n';
	std::cout << "PRINT" << '\n';//выводится  384
	std::cout << "PUSH 2.56" << '\n';
	std::cout << "SQRT" << '\n';
	std::cout << "PRINT" << '\n';//выводится  1.6
	std::cout << "DEFINE a 55" << '\n';
	std::cout << "PUSH a" << '\n';
	std::cout << "PRINT" << '\n';//выводится  55
	std::cout.rdbuf(prebuf);
	input.close();
	
	parserandwork(__argc, __argv);

	std::string str;

	output.open("output.txt");
	psbuf = output.rdbuf();
	std::cin.rdbuf(psbuf);
	std::getline(std::cin, str);
	ASSERT_TRUE(str == "5");
	std::getline(std::cin, str);
	ASSERT_TRUE(str == "10.768");
	std::getline(std::cin, str);
	ASSERT_TRUE(str == "0.768");
	std::getline(std::cin, str);
	ASSERT_TRUE(str == "768");
	std::getline(std::cin, str);
	ASSERT_TRUE(str == "384");
	std::getline(std::cin, str);
	ASSERT_TRUE(str == "1.6");
	std::getline(std::cin, str);
	ASSERT_TRUE(str == "55");
	std::cin.rdbuf(prebuf);
}
TEST_F(CalcTest, Excep)
{
	//________________________________________
	std::streambuf *psbuf, *prebuf;
	std::ofstream input;
	std::ifstream output;

	prebuf = std::cout.rdbuf();
	psbuf = input.rdbuf();// get file's streambuf
	if (__argc == 2)
	{
		input.open(__argv[1]);
		std::cout.rdbuf(psbuf);
	}
	std::cout << "PUSh 5" << '\n';//Invalid argument: PUSh 5
	std::cout << "Pop" << '\n';//Invalid argument: Pop
	std::cout << "PUSH a" << '\n';//Invalid argument : PUSH: invalid argument
	std::cout << "POP" << '\n';//Range error: POP: Stack is empty() or invalid argument
	std::cout << "+" << '\n';//Range error: Plus: Stack is empty() or invalid argument
	std::cout << "DEFINE 54 54" << '\n';//Invalid argument: Define: invalid argument
	std::cout.rdbuf(prebuf);
	input.close();

	parserandwork(__argc, __argv);

	std::string str;

	output.open("output.txt");
	psbuf = output.rdbuf();
	std::cin.rdbuf(psbuf);
	std::getline(std::cin, str);
	ASSERT_TRUE(str == "Invalid argument: PUSh 5");
	std::getline(std::cin, str);
	ASSERT_TRUE(str == "Invalid argument: Pop");
	std::getline(std::cin, str);
	ASSERT_TRUE(str == "Invalid argument: PUSH: invalid argument");
	std::getline(std::cin, str);
	ASSERT_TRUE(str == "Range error: POP: Stack is empty() or invalid argument");
	std::getline(std::cin, str);
	ASSERT_TRUE(str == "Range error: Plus: Stack is empty() or invalid argument");
	std::getline(std::cin, str);
	ASSERT_TRUE(str == "Invalid argument: Define: invalid argument");
	std::cin.rdbuf(prebuf);
}