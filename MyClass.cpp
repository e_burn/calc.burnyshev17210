#include "MyClass.h"
	void Push:: startwork(Context* cont)
	{
		//�� ������ ������� ������ first
		if (cont->string.empty() == true && cont->first != NULL)
		{
			cont->s.push(*(cont->first));
			return;
		}
		if (cont->string.empty() != true && cont->first == NULL && cont->mapa.count(cont->string) != 0)
		{
			cont->s.push(cont->mapa.at(cont->string));
			return;
		}
		throw std::invalid_argument("PUSH: invalid argument");
	}



	void Pop:: startwork(Context* cont)
	{
		//�������� ������� ����� � ����������
		if (cont->s.empty() != true && cont->string.empty() == true && cont->first == NULL)
		{
			cont->s.pop();
			return;
		}
		throw std::range_error("POP: Stack is empty() or invalid argument");
	}


	void Plus::startwork(Context* cont)
	{
		//�������� ����� � ����������
		if (cont->s.size() >= 2 && cont->string.empty() == true && cont->first == NULL)
		{
			double high = cont->s.top();
			cont->s.pop();
			double low = cont->s.top();
			cont->s.pop();
			cont->s.push(high + low);
			return;
		}
		throw std::range_error("Plus: Stack is empty() or invalid argument");
	}

	void Minus::startwork(Context* cont)
	{
		if (cont->s.size() >= 2 && cont->string.empty() == true && cont->first == NULL)
		{
			double high = cont->s.top();
			cont->s.pop();
			double low = cont->s.top();
			cont->s.pop();
			cont->s.push(low - high);
			return;
		}
		throw std::range_error("Minus: Stack is empty() or invalid argument");
	}


	void Mult::startwork(Context* cont)
	{
		if (cont->s.size() >= 2 && cont->string.empty() == true && cont->first == NULL)
		{
			double high = cont->s.top();
			cont->s.pop();
			double low = cont->s.top();
			cont->s.pop();
			cont->s.push(high*low);
			return;
		}
		throw std::range_error("Mult: Stack is empty() or invalid argument");
	}



	void Div::startwork(Context* cont)
	{
		if (cont->s.size() >= 2 && cont->string.empty() == true && cont->first == NULL)
		{
			double high = cont->s.top();
			cont->s.pop();
			double low = cont->s.top();
			cont->s.pop();
			cont->s.push(low / high);
			return;
		}
		throw std::range_error("Div: Stack is empty() or invalid argument");
	}




	void Sqrt::startwork(Context* cont)
	{
		if (cont->s.empty() != true && cont->string.empty() == true && cont->first == NULL)
		{
			double high = cont->s.top();
			cont->s.pop();
			cont->s.push(sqrt(high));
			return;
		}
		throw std::range_error("Sqrt: Stack is empty() or invalid argument");
	}



	void Print::startwork(Context* cont)
	{
		if (cont->s.empty() != true && cont->string.empty() == true && cont->first == NULL)
		{
			double high = cont->s.top();
			std::cout << high;
			std::cout << '\n';
			return;
		}
		throw std::range_error("Print: Stack is empty() or invalid argument");
	}



	void Define::startwork(Context* cont)
	{
		if (cont->string.empty() != true && cont->first != NULL)
		{
			cont->mapa.insert(std::pair<std::string, double>(cont->string, *cont->first));
			return;
		}
		throw std::invalid_argument("Define: invalid argument");
	}





	Comand_market* CreatorPush::create()
	{
		return new Push();
	}

	Comand_market* CreatorPop::create()
	{
		return new Pop();
	}



	Comand_market* CreatorPlus::create()
	{
		return new Plus();
	}



	Comand_market* CreatorMinus::create()
	{
		return new Minus();
	}



	Comand_market* CreatorMult::create()
	{
		return new Mult();
	}





	Comand_market* CreatorDiv::create()
	{
		return new Div();
	}



	Comand_market* CreatorSqrt::create()
	{
		return new Sqrt();
	}



	Comand_market* CreatorPrint::create()
	{
		return new Print();
	}



	Comand_market* CreatorDefine::create()
	{
		return new Define();
	}

	void parserandwork(int argc, char* argv[])
	{
		std::streambuf *psbuf;
		std::ifstream input;
		std::ofstream output;
		output.open("output.txt");
		psbuf = output.rdbuf();
		std::cout.rdbuf(psbuf);
		if (argc == 2)
		{
			input.open(argv[1]);
			psbuf = input.rdbuf();// get file's streambuf
			std::cin.rdbuf(psbuf);
		}
		//�������� Context � map ��� ����� �������
		Context* C = new (Context);
		CreatorPush* Pu = new CreatorPush;
		CreatorPop* Po = new CreatorPop;
		CreatorPlus* Pl = new CreatorPlus;
		CreatorMinus* Mi = new CreatorMinus;
		CreatorMult* Mu = new CreatorMult;
		CreatorDiv* Di = new CreatorDiv;
		CreatorSqrt* Sq = new CreatorSqrt;
		CreatorPrint* Pr = new CreatorPrint;
		CreatorDefine* De = new CreatorDefine;
		std::map <std::string, Creator*> MapFunc =
		{
			{ "PUSH", Pu },
			{ "POP", Po },
			{ "+", Pl },
			{ "-", Mi },
			{ "*", Mu },
			{ "/", Di },
			{ "SQRT", Sq },
			{ "PRINT", Pr },
			{ "DEFINE", De },
		};
		//���������� �� ������ ������ 
		std::string ourstring;//������ ������ 
		std::string comandstring;//������ � �������� ������� 
		std::regex func("^(([a-z]|[A-Z])+)|(\\+)|(\\-)|(\\*)|(\\/)");//��� �������
		std::regex param("[\\s]([a-z]|[A-Z])+");//��� ����������
		std::regex chislo("[\\s][\\d]+\\.*[\\d]*$");//��� �����(������ ���� � ����� ������)
		std::smatch allexpr;//��� ��������� ��� regexp
		std::smatch f;
		std::smatch ch;
		std::smatch par;
		std::string::size_type sz;
		//cin.getline(s, ������_�������);
		while (std::getline(std::cin, ourstring))
		{
			int find = 0;
			C->string.clear();//�������
			C->first = NULL;

			if (std::regex_search(ourstring, allexpr, func) != 0)//�������
			{
				std::regex_search(ourstring, f, func);
				comandstring = f[0];
				find++;
			}

			if (std::regex_search(ourstring, allexpr, param) != 0)//��������
			{
				std::regex_search(ourstring, par, param);
				C->string = par[0];
			}

			if (std::regex_search(ourstring, allexpr, chislo) != 0)//�����
			{
				std::regex_search(ourstring, ch, chislo);
				double* k = new(double);
				*k = std::stod(ch[0], &sz);;
				C->first = k;
			}

			try
			{
				if (find != 0 && MapFunc.count(comandstring) != 0)//�������� ���� �� ����� �������
				{
					Comand_market* Com = MapFunc.at(comandstring)->create();
					Com->startwork(C);
				}
				else
				{
					comandstring.append(" - NOT ANDERSTAND");
					throw std::invalid_argument(ourstring);
				}
			}
			catch (const std::invalid_argument& ia) {
				std::cout << "Invalid argument: " << ia.what() << '\n';
			}

			catch (const std::range_error& ia) {
				std::cout << "Range error: " << ia.what() << '\n';
			}
		}
		output.close();
	}