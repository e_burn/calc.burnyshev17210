#include <iostream>
#pragma warning(disable : 4996)
#include <string>
#include<conio.h>
#include <map>
#include <fstream>
#include <cctype>	
#include <utility>
#include <vector>
#include <regex>
#include <stack>
#include <stdexcept>
#include <cmath>
class Context
{
	//������ ��������� ���� � std::map<std::string, double>
	//1)stack
public:
	std::stack <double> s;
	std::map<std::string, double> mapa;
	std::string string;//������ ��� ������� 
	double* first;
};
class Comand_market : public Context
{
public:
	virtual void startwork(Context* cont) = 0;
	virtual ~Comand_market() = default;
};
class Push : public  Comand_market
{
public:
	void startwork(Context* cont)override;
};
class Pop : public  Comand_market
{
public:
	void startwork(Context* cont)override;
};
class Plus : public  Comand_market
{
public:
	void startwork(Context* cont)override;
};
class Minus : public  Comand_market
{
public:
	void startwork(Context* cont)override;
};
class Mult : public  Comand_market
{
public:
	void startwork(Context* cont)override;
};
class Div : public  Comand_market
{
public:
	void startwork(Context* cont)override;
};
class Sqrt : public  Comand_market
{
public:
	void startwork(Context* cont)override;
};
class Print : public  Comand_market
{
public:
	void startwork(Context* cont)override;
};
class Define : public  Comand_market
{
public:
	void startwork(Context* cont)override;
};
class Creator
{
public:
	virtual Comand_market* create() = 0;
	virtual ~Creator() = default;
};
class CreatorPush : public  Creator
{
public:
	Comand_market* create()override;
};
class CreatorPop : public  Creator
{
public:
	Comand_market* create()override;
};
class CreatorPlus : public  Creator
{
public:
	Comand_market* create()override;
};
class CreatorMinus : public  Creator
{
public:
	Comand_market* create()override;
};
class CreatorMult : public  Creator
{
public:
	Comand_market* create()override;
};
class CreatorDiv : public  Creator
{
public:
	Comand_market* create()override;
};
class CreatorSqrt : public  Creator
{
public:
	Comand_market* create()override;
};
class CreatorPrint : public  Creator
{
public:
	Comand_market* create()override;
};
class CreatorDefine : public  Creator
{
public:
	Comand_market* create()override;
};
void parserandwork(int argc, char* argv[]);